package Gui;

import javax.swing.*;
import java.awt.*;
import Comunication.*;
import Entities.*;

/**
 * Game Desk class
 * @author Kamil Jeřábek
 */
public class GameDesk extends JPanel {
    private boolean canMove;
    private final Gui gui;
    
    public GameDesk(Gui gui){
        super(new GridLayout(15,15));
        this.gui = gui;
        canMove = true;
    }
    
    /**
     * Initialize Game Desk gui
     * @param m 
     */
    public void initUI(CommandMessage m){
        this.gui.getGamePanel().getCommandTextField().setEditable(true);
        this.removeAll();
        this.canMove = true;
        this.setLayout(new GridLayout(15,15));
        this.setPreferredSize(new Dimension(480,480));
        this.setMaximumSize(this.getPreferredSize());
        this.setMinimumSize(this.getPreferredSize());
        for (int i = 0; i < 225; i++) {
            DeskSquare square = new DeskSquare();
            this.add( square );
        }      
        this.revalidate();
    }
    
    /**
     * repaint whole game desk depends on data from server
     * @param m 
     */   
    public void repaintDesk(CommandMessage m){
        m.getGame().draw(this, m.getGame().getPlayer(gui.client.getID()));
        this.gui.getGamePanel().setKeyNumber(m.getGame().getPlayer(gui.client.getID()).getKeyCount());
        this.revalidate();
        this.repaint();
    }
    
    /**
     * if win then show statistics
     * @param m 
     */    
    public void showWin(CommandMessage m){
        this.canMove = false;
        gui.getGamePanel().getIndividualMessageLabel().setText("");
        int statusColor;
        this.removeAll();
        this.setLayout(new GridLayout(0,2,10,10));
        this.setPreferredSize(new Dimension(480,480));
        Player p;
        JPanel panelWin = new JPanel();
        JLabel tmpLabel;
        Color barva;
        
        panelWin.setPreferredSize(new Dimension(200,200));
        for(int i = 0; i < m.getGame().getPlayerListSize(); i++){
            p = m.getGame().getPlayerList().get(i);
            panelWin = new JPanel();
            panelWin.setPreferredSize(new Dimension(200,200));
            statusColor = p.getIdInGame();
            
            if(statusColor == 0){
                barva = Color.RED;
            }
            else if(statusColor == 1){
                barva = Color.CYAN;
            }
            else if(statusColor == 2){
                barva = Color.GREEN;
            }
            else {
                barva = Color.ORANGE;
            }
            
            panelWin.setBorder(BorderFactory.createMatteBorder(
                                    5, 0, 0, 0, barva));
            tmpLabel = new JLabel("<html><b>Name: </b>"+p.getName()+"<br />"+
                    "<b>Steps: </b>"+p.getSteps()+"<br />"+
                    "<b>In Game: </b>"+(int)(p.getTimeInGame()/60)+":"+(int)(p.getTimeInGame()%60)+" min"+
                    "</html>");
            tmpLabel.setPreferredSize(new Dimension(200,200));
            tmpLabel.setHorizontalAlignment(SwingConstants.CENTER);
            tmpLabel.setVerticalAlignment(SwingConstants.CENTER);
            panelWin.add(tmpLabel);
            this.add(panelWin);
        }
        
        this.gui.getGamePanel().getIndividualMessageLabel().setText("Time: "+(int)(m.getGame().getGameTime()/60)+
                ":"+(int)(m.getGame().getGameTime()%60)+" min");
        this.gui.getGamePanel().getCommandTextField().setEditable(false);
        this.revalidate();
        this.repaint();
    }
    
    /**
     * get if can move or not
     * @return can move
     */
    public boolean getCanMove(){
        return this.canMove;
    }
    
    /**
     * if die then show player statistics
     * @param m 
     */
    public void showDie(CommandMessage m){
        this.canMove = false;
        gui.getGamePanel().getGlobalMessageLabel().setText("");
        gui.getGamePanel().getIndividualMessageLabel().setText("");
        this.removeAll();
        this.setLayout(new GridLayout(0,1));
        this.setPreferredSize(new Dimension(480,480));
        JPanel panelDie = new JPanel();
        JLabel tmpLabel;
        panelDie.setPreferredSize(new Dimension(480,480));
        Player p = m.getGame().getPlayer(gui.client.getID());
        tmpLabel = new JLabel("<html>YOU DIED!<br /><b>Name: </b>"+p.getName()+"<br />"+
                    "<b>Steps: </b>"+p.getSteps()+"<br />"+
                    "<b>In Game: </b>"+(int)(p.getTimeInGame()/60)+":"+(p.getTimeInGame()%60)+" min"+
                    "</html>");
        tmpLabel.setPreferredSize(new Dimension(480,480));
        tmpLabel.setHorizontalAlignment(SwingConstants.CENTER);
        tmpLabel.setVerticalAlignment(SwingConstants.CENTER);
        panelDie.add(tmpLabel);
        this.add(panelDie);
        this.gui.getGamePanel().getIndividualMessageLabel().setText("You Died!");
        this.gui.getGamePanel().getCommandTextField().setEditable(false);        
        this.revalidate();
        this.repaint();
    }
}
