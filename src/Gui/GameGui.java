package Gui;

import javax.swing.*;
import java.awt.*;
import Comunication.CommandMessage;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * inGameGui class
 * @author Kamil Jeřábek
 */
public class GameGui extends JPanel implements KeyListener, MouseListener{
    private final Gui gui;
    private final GameDesk gameDeskPanel;
    private final JPanel statusBarPanel;
    private final JLabel globalGameMessageLabel;
    private JLabel individualGameMessageLabel;
    private final JLabel keyStatusLabel;
    private JTextField commandTextField;
    private final JButton quitButton;
    private final JLabel help;
    
    /**
     * get KeyStatusLabel
     * @return keyStatusLabel
     */
    public JLabel getKeyStatusLabel(){
        return this.keyStatusLabel;
    }
    
    /**
     * get GlobalMessageLabel
     * @return globalGameMessageLabel
     */
    public JLabel getGlobalMessageLabel(){
        return this.globalGameMessageLabel;
    }
    
    /**
     * get IndividualMessageLabel
     * @return individualGameMessageLabel
     */
    public JLabel getIndividualMessageLabel(){
        return this.individualGameMessageLabel;
    }
    
    /**
     * get GameDeskPanel
     * @return gameDeskPanel
     */
    public GameDesk getGameDeskPanel(){
        return this.gameDeskPanel;
    }
    
    /**
     * get CommandTextField
     * @return commandTextField
     */
    public JTextField getCommandTextField(){
        return this.commandTextField;
    }
    
    public GameGui(final Gui gui){
        super(new GridLayout(0,1));
        this.setPreferredSize(new Dimension(490,610));
        this.gui = gui;


        gameDeskPanel = new GameDesk(gui);
        gameDeskPanel.addMouseListener(this);
        gameDeskPanel.addKeyListener(this);
        statusBarPanel = new JPanel();
        globalGameMessageLabel = new JLabel("Global message", SwingConstants.LEFT);
        globalGameMessageLabel.setPreferredSize(new Dimension(180,30));
        individualGameMessageLabel = new JLabel("Individual message", SwingConstants.LEFT);
        individualGameMessageLabel.setPreferredSize(new Dimension(180,30));
        keyStatusLabel = new JLabel("Keys: 0", SwingConstants.RIGHT);
        keyStatusLabel.setPreferredSize(new Dimension(100,30));
        commandTextField = new JTextField(20);
        commandTextField.setPreferredSize(new Dimension(460,20));
        //commandTextField.addKeyListener(this);
        quitButton = new JButton("quit");
        help = new JLabel("?");
        help.setPreferredSize(new Dimension(20,20));
        help.setHorizontalAlignment(SwingConstants.CENTER);
        help.setVerticalAlignment(SwingConstants.CENTER);
        help.setToolTipText("<html><b>Help<br />"
                +"Click to command line to type commands<br />"
                + "or to game desk for bindings<br /><br />"
                + "Commands:</b><br />"
                +"<b>left</b> - turn left<br />"
                +"<b>right</b> - turn right<br />"
                +"<b>step</b> - make step ahead<br />"
                +"<b>take</b> - take key<br />"
                +"<b>open</b> - open gate<br /><br />"
                + "<b>Binds:</b><br />"
                + "<b>key: w</b> - go up<br />"
                + "<b>key: s</b> - go down<br />"
                + "<b>key: a</b> - go left<br />"
                + "<b>key: d</b> - go right<br />"
                + "<b>key: e</b> - take key<br />"
                + "<b>key: SPACE</b> - open gate<br />"                
                + "</html>");
        quitButton.setPreferredSize(new Dimension(80,30));
        
        statusBarPanel.setPreferredSize(new Dimension(480,30));
        statusBarPanel.setLayout(new BorderLayout(10,0));
        statusBarPanel.add(globalGameMessageLabel, BorderLayout.CENTER);
        statusBarPanel.add(individualGameMessageLabel, BorderLayout.LINE_START);
        statusBarPanel.add(keyStatusLabel, BorderLayout.LINE_END);
        statusBarPanel.setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, Color.DARK_GRAY));
        
        quitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CommandMessage m = new CommandMessage(gui.client.getGameID(), gui.client.getID(), "leave", false);
                gui.client.typeOut(m);
                gui.getCardLayoutGuiPanel().show(gui, "mainMenu");
                gui.mainMenuPanel.setMenuFrameSize();
            }
        });
        
        commandTextField.addActionListener(new java.awt.event.ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent e){
                String command = commandTextField.getText();
                if(command.equals("step") || command.equals("left") ||command.equals("right")
                        || command.equals("go") || command.equals("take") || command.equals("open")
                        || command.equals("stop")) {
                    CommandMessage m = new CommandMessage(gui.client.getGameID(), gui.client.getID(), command, true);
                    gui.client.typeOut(m);

                }
                else{
                    individualGameMessageLabel.setText("Unknown Command!");
                    
                }
                commandTextField.setText("");
            }
        });
        


        GroupLayout jPanel1Layout = new GroupLayout(this);
        this.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addComponent(gameDeskPanel, 480, 480, 480)
                            .addComponent(statusBarPanel, 480, 480, 480)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(commandTextField, 460, 460, 460)
                                .addComponent(help, 20, 20, 20)
                            )
                            .addGap(10, 10, 10)
                            .addComponent(quitButton, 80, 80, 80)
                        )
        );
   
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(5, 5, 5)
                            .addComponent(gameDeskPanel, 480, 480, 480)
                            .addComponent(statusBarPanel, 30, 30, 30)
                            .addGap(5,5,5)
                            .addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(commandTextField, 20, 20, 20)
                                .addComponent(help, 20, 20, 20)
                            )
                            .addGap(10, 10, 10)
                            .addComponent(quitButton, 30, 30, 30)
                            .addGap(10, 10, 10)
                    )
        );
        revalidate();
    }
    
    /**
     * create and show game desk and set color of statusbar
     * @param m 
     */
    public void createAndShowGame(CommandMessage m){
        gameDeskPanel.initUI(m);
        setStatusBarColor(m.getGame().getPlayer(gui.client.getID()).getIdInGame());
    }
    
    /**
     * change global message text (depends on reaction from server)
     * @param message 
     */
    public void addGlobalGameMessageTextArea(String message){
        this.globalGameMessageLabel.setText(message);
    }
    
    /**
     * change individual message text (depends on reaction from server)
     * @param message 
     */
    public void addIndividualGameMessageTextArea(String message){
        this.individualGameMessageLabel.setText(message);
    }
    
    /**
     * set color of statusbar
     * @param i 
     */
    public void setStatusBarColor(int i){
        if(i == 0){
            this.statusBarPanel.setBackground(Color.RED);
        }
        else if(i == 1){
            this.statusBarPanel.setBackground(Color.CYAN);
        }
        else if(i == 2){
            this.statusBarPanel.setBackground(Color.GREEN);
        }
        else if(i == 3){
            this.statusBarPanel.setBackground(Color.ORANGE);
        }

    }
    
    /**
     * set number of key that player have (depends on reaction from server)
     * @param i 
     */
    public void setKeyNumber(int i){
        this.keyStatusLabel.setText("Keys: "+i+" ");
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
        CommandMessage m = null;
        if(this.gameDeskPanel.getCanMove()){
            if (e.getKeyCode() == KeyEvent.VK_W) {
                m = new CommandMessage(gui.client.getGameID(), gui.client.getID(), "stepUp", true); 
            }
            else if (e.getKeyCode() == KeyEvent.VK_S) {
                m = new CommandMessage(gui.client.getGameID(), gui.client.getID(), "stepDown", true);            
            }
            else if (e.getKeyCode() == KeyEvent.VK_A) {
                m = new CommandMessage(gui.client.getGameID(), gui.client.getID(), "stepLeft", true);            
            }
            else if (e.getKeyCode() == KeyEvent.VK_D) {
                m = new CommandMessage(gui.client.getGameID(), gui.client.getID(), "stepRight", true);            
            }
            else if (e.getKeyCode() == KeyEvent.VK_E) {
                m = new CommandMessage(gui.client.getGameID(), gui.client.getID(), "take", true);            
            }
            else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                m = new CommandMessage(gui.client.getGameID(), gui.client.getID(), "open", true);            
            }
            if(m != null){
                gui.client.typeOut(m);
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        gameDeskPanel.requestFocus();
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
