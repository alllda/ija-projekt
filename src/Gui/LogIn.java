package Gui;

import javax.swing.*;
import java.awt.*;
/**
 * LogIn class
 * @author Kamil Jeřábek
 */
public class LogIn extends JPanel {
    private final Gui gui;
    private final JLabel loginLabel;
    private JTextField loginTextField;
    private final JButton loginButton;
    private final JLabel setServerLabel;
    private final JLabel setPortLabel;
    private JTextField setServerTextField;
    private JTextField setPortTextField;
    
    public LogIn(final Gui gui){
        super(new GridLayout(0,2));
        this.gui = gui;
        
        loginLabel = new JLabel("Login");
        loginTextField = new JTextField();
        loginButton = new JButton("connect");
        setServerLabel = new JLabel("Set server name:");
        setPortLabel = new JLabel("Set port name:");
        setServerTextField = new JTextField();
        setPortTextField = new JTextField();
        /** Just temporary for testing */
        setServerTextField.setText("localhost");
        setPortTextField.setText("9999");
        loginTextField.setText("Temp");
        
        loginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    gui.client.ConnectToServer(setServerTextField.getText(), Integer.parseInt(setPortTextField.getText()) , loginTextField.getText());
                }
                catch(NumberFormatException ex){
                    System.exit(2);
                }
                
                gui.getCardLayoutGuiPanel().show(gui, "mainMenu");
                gui.mainMenuPanel.setMenuFrameSize();
            }
        });
        
        add(setServerLabel);
        add(setServerTextField);
        add(setPortLabel);
        add(setPortTextField);
        add(loginLabel);
        add(loginTextField);
        add(loginButton);
    }
    
}
