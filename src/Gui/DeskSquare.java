package Gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * DeskSquare class
 * @author Kamil Jeřábek
 */
public class DeskSquare extends JPanel {
    private BufferedImage img;
    private final JLabel label;
    
    public DeskSquare(){
        super();
        this.setPreferredSize(new Dimension(32,32));
        this.setMaximumSize(this.getPreferredSize());
        this.setMinimumSize(this.getPreferredSize());
        this.setLayout(new BorderLayout());
        label = new JLabel();
        this.label.setSize(new Dimension(32,32));
        this.add(label);
    }
    
    /**
     * get Desk Square Label
     * @return label
     */
    public JLabel getDeskSquareLabel(){
        return this.label;
    }
    
    /**
     * load Background texture of desk square
     * @param path 
     */
    public void loadBackground(String path) {
        try {
            img = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * add entity (policeman or player) to desksquare
     * @param path 
     */
    public void addEntity(String path){
        this.label.setIcon(new ImageIcon(path));
    }
    
    /**
     * add tooltip for player on desksquare
     * @param text 
     */
    public void addTooltip(String text){
        this.label.setToolTipText(text);
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, getWidth(), getHeight(), this);
    }
}
