package Gui;

import projekt.Client;
import javax.swing.*;
import java.awt.*;

/**
 * Gui class
 * @author Kamil Jeřábek
 */
public class Gui extends JPanel {
    protected Client client;
    private final JFrame frame;
    private final CreateGame createGamePanel;
    private final JoinGame joinGamePanel;
    private final GameGui gamePanel;
    protected Menu mainMenuPanel;
    protected LogIn loginPanel;
    private final CardLayout cardLayoutGuiPanel;
    
    public Gui(Client client){
        super();
        this.client = client;
        frame = new JFrame("IJA - Client");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setMinimumSize(new Dimension(300,150));
        
        cardLayoutGuiPanel = new CardLayout();       
        
        //create panels
        loginPanel = new LogIn(this);
        mainMenuPanel = new Menu(this);
        createGamePanel = new CreateGame(this);
        joinGamePanel = new JoinGame(this);
        gamePanel = new GameGui(this);

        this.setLayout(cardLayoutGuiPanel);
        cardLayoutGuiPanel.show(this, "Gui");
        this.add(loginPanel, "login");
        this.add(mainMenuPanel, "mainMenu");
        this.add(createGamePanel, "createGame");
        this.add(joinGamePanel, "joinGame");
        this.add(gamePanel, "game");
        
        frame.setContentPane(this);
        
        frame.setResizable(false);
        frame.setVisible(true);
    }
    
    /**
     * get Frame
     * @return frame
     */
    public JFrame getFrame(){
        return this.frame;
    }
    
    /**
     * get create game panel
     * @return createGamePanel
     */
    public CreateGame getCreateGamePanel(){
        return this.createGamePanel;
    }
    
    /**
     * get Join Game Panel
     * @return joinGamePanel
     */
    public JoinGame getJoinGamePanel(){
        return this.joinGamePanel;
    }
    
    /**
     * get Game Panel
     * @return gamePanel
     */
    public GameGui getGamePanel(){
        return this.gamePanel;
    }
    
    /**
     * get Card Layout Gui Panel
     * @return cardLayoutGuiPanel
     */
    public CardLayout getCardLayoutGuiPanel(){
        return this.cardLayoutGuiPanel;
    }
}
