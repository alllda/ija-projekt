package Gui;

import javax.swing.*;
import java.awt.*;
import Comunication.CommandMessage;

/**
 * CreateGame class
 * @author Kamil Jeřábek
 */
public class CreateGame extends JPanel {
    private final Gui gui;
    private final JButton create;
    private final JButton backToMenu;
    private JList gameChoiceList;
    private final DefaultListModel listModel;
    private JTextField gameName;
    private final JLabel labelGameName;
    private final JLabel labelRefresh;
    private final JLabel pickMapLabel;
    private JSlider refreshSlider;
    private JScrollPane listScrollPane;
    
    public CreateGame(final Gui gui){
        super(new GridLayout(0,2));
        this.gui = gui;
        
        listModel = new DefaultListModel();
        create = new JButton("create");
        backToMenu = new JButton("Menu");
        gameChoiceList = new JList(listModel);
        labelGameName = new JLabel("Game name:");
        pickMapLabel = new JLabel("Pick map:");
        gameName = new JTextField();
        labelRefresh = new JLabel("Refresh:");
        refreshSlider = new JSlider(JSlider.HORIZONTAL, 500, 5000, 3000);
        
        refreshSlider.setMajorTickSpacing(1000);
        refreshSlider.setMinorTickSpacing(100);
        refreshSlider.setPaintTicks(true);
        
        gameChoiceList = new JList(listModel);
        gameChoiceList.setSelectionMode(
            ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        gameChoiceList.setSelectedIndex(0);
        //gameChoiceList.addListSelectionListener();
        listScrollPane = new JScrollPane(gameChoiceList);
        
        /*
         *vytvoreni hry
        */
        create.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(!gameChoiceList.isSelectionEmpty() && !gameName.getText().equals("")){
                    CommandMessage m = new CommandMessage( 0, gui.client.getID(), "game", false);
                    m.setName(gameName.getText());
                    m.setMap("examples/maps/"+gameChoiceList.getSelectedValue()+".txt");
                    m.setRefreshTime((refreshSlider.getValue()));
                    gui.client.typeOut(m);
                }
                
                gameName.setText("");
            }
        });
        
        backToMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gui.getCardLayoutGuiPanel().show(gui, "mainMenu");
                gui.mainMenuPanel.setMenuFrameSize();
            }
        });
        
        GroupLayout jPanel1Layout = new GroupLayout(this);
        this.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup()
                .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20)
                        .addComponent(pickMapLabel, 100, 100, 100)
                        .addGap(20)
                        .addComponent(listScrollPane, 150, 150, 150)
                )
    
                .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20)
                        .addComponent(labelRefresh, 100, 100, 100)
                        .addGap(20)
                        .addComponent(refreshSlider, 150, 150, 150)
                )

                .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20)
                        .addComponent(labelGameName, 100, 100, 100)
                        .addGap(20)
                        .addComponent(gameName, 150, 150, 150)
                )

                .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20)
                        .addComponent(backToMenu, 100, 100, 100)
                        .addGap(70)
                        .addComponent(create, 100, 100, 100)
                )
  
        );
   
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createSequentialGroup()
                .addGap(20)
                .addGroup(jPanel1Layout.createParallelGroup()
                        .addComponent(pickMapLabel, 20, 20, 20)
                        .addComponent(listScrollPane, 100, 100, 100)
                )
                .addGap(15)
                .addGroup(jPanel1Layout.createParallelGroup()
                        .addComponent(labelRefresh, 30, 30, 30)
                        .addComponent(refreshSlider, 30, 30, 30)
                )
                .addGap(15)
                .addGroup(jPanel1Layout.createParallelGroup()
                        .addComponent(labelGameName, 20, 20, 20)
                        .addComponent(gameName, 20, 20, 20)
                )
                .addGap(20)
                .addGroup(jPanel1Layout.createParallelGroup()
                        .addComponent(backToMenu, 30, 30, 30)
                        .addComponent(create, 30, 30, 30)
                )
        );
    }
    
    /**
     * fill the Map List (depends on reaction from server)
     * @param m 
     */
    public void fillMapList(CommandMessage m){
        this.listModel.clear();
        gui.getFrame().setSize(new Dimension(320,300));
        for (int i = 0; i < m.getMapsList().length; i++){
            String item = m.getMapsList()[i];
            this.listModel.addElement(item.replace("examples/maps/","").replaceAll(".txt$", ""));
        }
    }
    
}
