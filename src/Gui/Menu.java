package Gui;

import javax.swing.*;
import java.awt.*;
import Comunication.CommandMessage;

/**
 * Menu class
 * @author Kamil Jeřábek
 */
public class Menu extends JPanel {
    private final Gui gui;
    protected JButton createButton;
    protected JButton joinButton;
    protected JButton settingButton;
    protected JButton quitButton;
    protected JLabel menuLabel;
    
    
    public Menu(final Gui gui){
        super(new GridLayout(0,1));
        this.gui = gui;
        
        createButton = new JButton("Create");
        joinButton = new JButton("Join");
        quitButton = new JButton("Quit");
        menuLabel = new JLabel("MENU");
        
        createButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CommandMessage m = new CommandMessage(0,gui.client.getID(),"mapList",false);
                gui.client.typeOut(m);
                gui.getCardLayoutGuiPanel().show(gui, "createGame");
            }
        });
        
        joinButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CommandMessage m = new CommandMessage(0,gui.client.getID(),"gameList",false);
                gui.client.typeOut(m);
                gui.getCardLayoutGuiPanel().show(gui, "joinGame");
            }
        });
        
        
        quitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CommandMessage m = new CommandMessage(0,gui.client.getID(),"quit",false);
                gui.client.typeOut(m);
                System.exit(0);
            }
        });
        
        GroupLayout jPanel1Layout = new GroupLayout(this);
        this.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createSequentialGroup()
                    .addGap(70)
                    .addGroup(jPanel1Layout.createParallelGroup()
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(55)
                            .addComponent(menuLabel, 50, 50, 50)
                        )
                        .addComponent(createButton, 150, 150, 150)
                        .addComponent(joinButton, 150, 150, 150)
                        .addComponent(quitButton, 150, 150, 150)
                    )
        );
   
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createSequentialGroup()
                .addGap(20)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(menuLabel, 30, 30, 30)
                    .addGap(20)
                    .addComponent(createButton, 30, 30, 30)
                    .addGap(10)
                    .addComponent(joinButton, 30, 30, 30)
                    .addGap(70)
                    .addComponent(quitButton, 30, 30, 30)
                )
        );
    }  
    
    /**
     * set size of frame for menu
     */
    public void setMenuFrameSize(){
        this.gui.getFrame().setSize(new Dimension(100,320));
    }
}
