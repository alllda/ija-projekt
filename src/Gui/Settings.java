/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Gui;

import projekt.Client;
import javax.swing.*;
import java.awt.*;
import Comunication.CommandMessage;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
/**
 *
 * @author xjerab18
 */
public class Settings extends JPanel {
    private Gui gui;
    private JButton cancelButton;
    private JButton applyButton;
    private JLabel setServerLabel;
    private JLabel setPortLabel;
    private JTextField setServerTextField;
    private JTextField setPortTextField;
    
    
    public Settings(final Gui gui){
        super(new GridLayout(0,2));
        this.gui = gui;
        
        cancelButton = new JButton("Cancel");
        applyButton = new JButton("Apply");
        setServerLabel = new JLabel("Set server name:");
        setPortLabel = new JLabel("Set port name:");
        setServerTextField = new JTextField();
        setPortTextField = new JTextField();
        
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gui.getCardLayoutGuiPanel().show(gui, "mainMenu");
            }
        });
        
        applyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gui.getCardLayoutGuiPanel().show(gui, "mainMenu");
            }
        });
        
        add(setServerLabel);
        add(setServerTextField);
        add(setPortLabel);
        add(setPortTextField);
        add(applyButton);
        add(cancelButton);
        
    }
    
}
