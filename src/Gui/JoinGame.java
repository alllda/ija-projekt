package Gui;

import Games.*;
import javax.swing.*;
import java.awt.*;
import Comunication.CommandMessage;

/**
 * JoinGame class
 * @author Kamil Jeřábek
 */
public class JoinGame extends JPanel {
    private final Gui gui;
    private final JButton backToMenu;
    private final JButton joinGame;
    private final JButton refresh;
    private JList choiceGame;
    private final JLabel choiceGameLabel;
    private final DefaultListModel listModel;
    private int []id;
    private JScrollPane listScrollPane;
    
    public JoinGame(final Gui gui){
        super(new GridLayout(0,1));
        this.gui = gui;
        listModel = new DefaultListModel();

        
        backToMenu = new JButton("Menu");
        joinGame = new JButton("Join");
        refresh = new JButton("Refresh");
        choiceGameLabel = new JLabel("Pick game:");
        choiceGame = new JList(listModel);
        
        backToMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gui.getCardLayoutGuiPanel().show(gui, "mainMenu");
                gui.mainMenuPanel.setMenuFrameSize();
            }
        });
       
        joinGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(!choiceGame.isSelectionEmpty()){
                    CommandMessage m = new CommandMessage(id[choiceGame.getSelectedIndex()], gui.client.getID(), "join", false);
                    //System.out.println(id[choiceGame.getSelectedIndex()]);
                    gui.client.typeOut(m);
                }
            }
        });
        
        refresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CommandMessage m = new CommandMessage(0,gui.client.getID(),"gameList",false);
                gui.client.typeOut(m);
            }
        });
        
        choiceGame.setSelectedIndex(0);
        choiceGame.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listScrollPane = new JScrollPane(choiceGame);
        
        GroupLayout jPanel1Layout = new GroupLayout(this);
        this.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup()
                    .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(20)
                            .addGroup(jPanel1Layout.createParallelGroup()
                                .addComponent(choiceGameLabel, 150, 150, 150)
                                .addGap(20)
                                .addComponent(listScrollPane, 250, 250, 250)
                            )
                            .addGap(20)
                            .addGroup(jPanel1Layout.createParallelGroup()
                                    .addComponent(joinGame, 100, 100, 100)
                                    .addComponent(refresh, 100, 100, 100)
                                    .addComponent(backToMenu, 100, 100, 100)
                            )
                    )
        );
   
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createSequentialGroup()
                .addGap(10)
                .addComponent(choiceGameLabel, 30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup()
                    .addComponent(listScrollPane, 300, 300, 300)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(joinGame, 30, 30, 30)
                            .addGap(10)
                            .addComponent(refresh, 30, 30, 30)
                            .addGap(200)
                            .addComponent(backToMenu, 30, 30, 30)
                    )
                )
        );

        revalidate();
    }
    
    /**
     * fill game list by running games
     * @param m 
     */
    public void fillGameList(CommandMessage m){
        gui.getFrame().setSize(new Dimension(420, 390));
        id = new int[m.getGamesList().size()];
        listModel.clear();
        for (int i = 0, j = 0; i < m.getGamesList().size(); i++){
            Game item = m.getGamesList().get(i);
            if(item.getPlayerListSize() < 4){
                id[j] = item.getID();
                listModel.addElement(item.getGameName());
                j++;
            }
        }
        revalidate();
    }
}
