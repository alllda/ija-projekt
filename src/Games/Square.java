

package Games;
import SquareObjects.*;
import Entities.*;
import Gui.*;
import java.io.Serializable;
/**
 * Square class
 * @author Aleš Raszka
 */
public class Square implements Serializable{
    private SquareObject object;
    private Entity entity;
    
    public Square(SquareObject object){
        this.object = object;
        this.entity = null;
    }
    public Square(SquareObject object, Entity e){
        this.object = object;
        this.entity = e;
    }
    /**
     * Set new object to square
     * @param object new object
     */
    public void newObject(SquareObject object){
        this.object = object;
    }
    /**
     * Text draw to console
     */
    public void textDraw(){
        if(entity != null)
            entity.textDraw();
        else
            object.textDraw();
    }
    /**
     * Draw to gui
     * @param panel panel
     */
    public void draw(DeskSquare panel){

        object.draw(panel);
        if(entity != null){
            entity.draw(panel);
        }
    }
    /**
     * Get key
     * @return key
     */
    public boolean GetKey(){
        return object.GetKey();
    }
    /**
     * Open object
     * @return is Open
     */
    public boolean Open(){
        return object.Open();
    }
    /**
     * Can object be open?
     * @return bool
     */
    public boolean CanBeOpen(){
        return object.CanBeOpen();
    }
    /**
     * Is finish block
     * @return bool
     */
    public boolean isFinish(){
        return object instanceof Finish;
    }
    
    /**
     * Can seize by entity
     * @return bool
     */
    public boolean CanSeize(){
        if(entity == null){
            return object.CanSeize();
        }
        else
            return false;
    }
    /**
     * Seize by entity
     * @param entity
     * @return bool
     */
    public boolean Seize(Entity entity){
        if(this.CanSeize()){
            this.entity = entity;
            return true;
        }
        else
            return false;
            
    }
    
    /**
     * Leave entity
     * @return entity
     */
    public Entity leave(){
        Entity e = this.entity;
        this.entity = null;
        return e;
    }
    /**
     * Get entity from block
     * @return entity
     */
    public Entity getEntity(){
        return entity;
    }
    /**
     * Get player
     * @return player
     */
    public Player getPlayer(){
        if(entity != null)
            return entity.getPlayer();
        else
            return null;
    }
    
    
}
