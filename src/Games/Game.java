package Games;

import Entities.Player;
import SquareObjects.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import Comunication.CommandMessage;
import Entities.Policeman;
import java.util.logging.Level;
import java.util.logging.Logger;

import Gui.*;
import java.io.Serializable;
import java.util.ArrayList;


/**
 * Game class
 * @author Aleš Raszka
 */
public class Game extends Thread implements Serializable{
    private static int count = 0;
    private final int id;
    private Square squares[][];
    private int row;
    private int col;
    private final String name;
    private Player player;
    private List<Player> players;
    private List<Square> finishBlock;
    private final List<Policeman> policemans;
    private final int refreshTime;
    private int playerCount;
    private int whereToPlace;
    private boolean gameRunning;
    private boolean freeSlots[] = new boolean[4];
    private final double startTime;
    private double gameTime;
    
    public Game(String map, String name, Player player, int refreshTime)throws IOException{
        this.whereToPlace = 0;
        gameRunning = true;
        for(int i = 0; i < 4; i++)
            freeSlots[i] = true;
        player.setIdInGame(0);
        player.resetAll();
        freeSlots[0] = false;
        policemans = new ArrayList<>();
        loadMap(map);
        this. name = name;        
        id = count++;
        this.refreshTime = refreshTime;
        players = new ArrayList<>();
        players.add(player);
        addPlayerToMap(player);
        playerCount = 1;
        CommandMessage m = new CommandMessage(this.id, 0, "newGame", true);
        m.setGame(this);
        player.sendMessage(m);
        m = new CommandMessage(this.id, 0, "update", true);
        m.setGame(this);
        sendUpdateToAll(m);
        m = new CommandMessage("globalMessage"," Game: "+this.getGameName());
        sendUpdateToAll(m);
        startTime = System.nanoTime();
        //System.out.println("Game: "+ this.getGameName()+":: Player: " + player.getName() + " create game");
        //System.out.println("Refresh: "+refreshTime);
        
    }
    
    /**
     * Get count of player in game
     * @return player count
     */
    public int getPlayerListSize(){
        return this.players.size();
    }
    
    /**
     * Get list of players
     * @return player List
     */
    public List<Player> getPlayerList(){
        return this.players;
    }
    
    /**
     * Get list of policeman
     * @return policeman list
     */
    public List<Policeman> getPolicemanList(){
        return policemans;
    }
    /**
     * Get time when game started
     * @return start time
     */
    public double getStartTime(){
        return startTime;
    }

    
    
    @Override
    public void run(){
        int i =0;
        while(gameRunning)
        {
            try {
                Thread.sleep(refreshTime);
            } catch (InterruptedException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
            CommandMessage m = new CommandMessage(this.id, 0, "update", true);
            m.setGame(this);
            m.setName("Pohnuti policajta");
            // Move all players
            for(Player p : players){
                if(p.isGoing()){
                    try {
                        makeStepPlayer(p, m);
                    } catch (IOException ex) {
                        Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }            
            }
            // move all policemen
            for(Policeman policeman : policemans){
                try {
                    policeman.changeDirection();
                    makeStepPoliceman(policeman, m);
                } catch (IOException ex) {
                    Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if(gameRunning)
                try {
                    sendUpdateToAll(m);
                    //this.textDraw();
            } catch (IOException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
            //this.textDraw();
        }
    }
    /**
     * Get id of game
     * @return game id
     */
    public int getID(){
        return id;
    }
    
    /**
     * Player join into game
     * @param p player
     * @throws IOException 
     */
    public synchronized void joinPlayer(Player p) throws IOException{
        if(playerCount <4){
            players.add(p);
            for(int i = 0; i < 4;i++){
                if(freeSlots[i]){
                    p.setIdInGame(i);
                    freeSlots[i] = false;
                    break;
                }
            }
            p.setStartTime();
            p.stop();
            p.resetkeyCount();
            p.resetAll();
            playerCount++;
            addPlayerToMap(p);
            CommandMessage m = new CommandMessage(this.id, 0, "newGame", true);
            m.setGame(this);
            p.sendMessage(m);
            m = new CommandMessage(this.id, 0, "update", true);
            m.setGame(this);
            sendUpdateToAll(m);
            m = new CommandMessage("globalMessage", p.getName()+" joined to game");
            sendUpdateToAll(m);
            //System.out.println("Game: "+ this.getGameName()+":: Player: " + p.getName() + " join to game");
        }
        else{
            CommandMessage m = new CommandMessage(this.id, 0, "fullGame", true);
            p.sendMessage(m);
        }
        
    }

    /**
     * Load map from file
     * @param map path to file
     * @throws FileNotFoundException
     * @throws IOException 
     */
    private void loadMap(String map) throws FileNotFoundException, IOException{
        BufferedReader br = new BufferedReader(new FileReader(map));
        try {
            finishBlock = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            // first line = rows
            String line = br.readLine();
            this.row = Integer.parseInt(line);
            // second line = cols
            line = br.readLine();
            this.col = Integer.parseInt(line);
            squares = new Square[col][row];
            for(int i = 0; i < row;i++){
                line = br.readLine();
                for(int j=0; j < col; j++){
                    char c = line.charAt(j);
                    if(c == '#')
                        squares[i][j] = new Square(new Wall());
                    else if(c == '.')
                        squares[i][j] = new Square(new Path());
                    else if(c == 'G')
                        squares[i][j] = new Square(new Gate());
                    else if(c == 'k')
                        squares[i][j] = new Square(new Key());
                    else if(c == 'F'){
                        squares[i][j] = new Square(new Finish());
                        finishBlock.add(squares[i][j]);
                    }
                    else if(c == 'P'){
                        squares[i][j] = new Square(new Path());
                        Policeman policeman = new Policeman();
                        policeman.setPossition(i, j);
                        policemans.add(policeman);
                        squares[i][j].Seize(policeman);
                    }
                }
                
            }
            
        } finally {
            br.close();
        }
    }
    /**
     * Find place where to add Player to map    
     * @param p Player
     */
    private void addPlayerToMap(Player p){
        int place = p.getIdInGame();
        if(place == 0){
            // left top
            for(int y = 0; y < col;y++){
                for(int x = 0; x < row; x++){
                    if(squares[y][x].CanSeize()){
                        squares[y][x].Seize(p);
                        whereToPlace = (whereToPlace +1) % 4;
                        p.setPossition(y, x);
                        return;
                    }
                }
            }
        }
        //left down
        else if(place == 1){
            for(int y = col-1; y >= 0;y--){
                for(int x = 0; x < row; x++){
                    if(squares[y][x].CanSeize()){
                        squares[y][x].Seize(p);
                        whereToPlace = (whereToPlace +1) % 4;
                        p.setPossition(y, x);
                        return;
                    }
                }
            }
        }
        // right top
        else if(place == 2){
            for(int y = 0; y < col;y++){
                for(int x = row-1; x >= 0; x--){
                    if(squares[y][x].CanSeize()){
                        squares[y][x].Seize(p);
                        whereToPlace = (whereToPlace +1) % 4;
                        p.setPossition(y, x);
                        return;
                    }
                }
            }
        }
        // right down
        else if(place == 3){
            for(int y = col-1; y >= 0;y--){
                for(int x = row-1; x >=0; x--){
                    if(squares[y][x].CanSeize()){
                        squares[y][x].Seize(p);
                        whereToPlace = (whereToPlace +1) % 4;
                        p.setPossition(y, x);
                        return;
                    }
                }
            }
        }
    }
    
    /**
     * Draw game to GUI
     * @param gameDesk gamedesk gui component
     * @param player player
     */
    public void draw(GameDesk gameDesk, Player player){
        int i = 0;
        DeskSquare panel;
        int xStart = 0, yStart = 0, xEnd = 0, yEnd = 0;
        // player must be in center
        if(player.getX() < 7){
            xStart = 0;
            xEnd = 15;
        }
        else if(player.getX() > (row - 8)){
            xStart = row - 15;
            xEnd = row;
        }
        else{
            xStart = player.getX() - 7;
            xEnd = player.getX() + 8;
        }
        
        if(player.getY() < 7){
            yStart = 0;
            yEnd = 15;
        }
        else if(player.getY() > (col - 8)){
            yStart = col - 15;
            yEnd = col;
        }
        else{
            yStart = player.getY() - 7;
            yEnd = player.getY() + 8;
        }
        
        for(int y = yStart; y < yEnd; y++){
            for(int x = xStart; x < xEnd; x++, i++){
                panel = (DeskSquare)gameDesk.getComponent(i);
                panel.getDeskSquareLabel().setIcon(null);
                panel.getDeskSquareLabel().revalidate();
                squares[y][x].draw(panel);
            }
        }
        
    }
    /**
     * Print game desk to console
     */
    public void textDraw(){
        for(int y = 0; y < col; y++){
            for(int x = 0; x < row; x++){
                squares[y][x].textDraw();
                System.out.print(" ");
            }
            System.out.println(" ");
        }
    }
    
    /**
     * Get command from player
     * @param command command from player
     * @return bool
     * @throws IOException 
     */
    public synchronized boolean Command(CommandMessage command) throws IOException{
        int playerID = command.getPlayerID();
        Player p = getPlayer(playerID);
        if(p == null)
            return false;
        String message = command.getCommand();
        CommandMessage m = new CommandMessage(this.id, 0, "update", true);
        m.setGame(this);
        if(message.equals("left")){
            p.turnLeft();
            //System.out.println(p.getName() + " turn left");
            sendUpdateToAll(m);
            m = new CommandMessage("localMessage", "You turn left");
            p.sendMessage(m);
            return true;
        }
        else if(message.equals("right")){
            p.turnRight();
            //System.out.println(p.getName() + " turn right");
            sendUpdateToAll(m);
            m = new CommandMessage("localMessage", "You turn right");
            p.sendMessage(m);
            return true;
        }
        else if(message.equals("go")){
            p.go();
            m = new CommandMessage("localMessage", p.getName()+" is in move");
            p.sendMessage(m);
        }
        else if(message.equals("stop")){
            p.stop();
            m = new CommandMessage("localMessage", p.getName()+" stop");
            p.sendMessage(m);
        }
        else if(message.equals("step")){
            makeStepPlayer(p, m);
            
        }
        else if(message.equals("stepLeft")){
            p.toLeft();
            makeStepPlayer(p, m);
        }
        else if(message.equals("stepRight")){
            p.toRight();
            makeStepPlayer(p, m);
        }
        else if(message.equals("stepUp")){
            p.up();
            makeStepPlayer(p, m);
        }
        else if(message.equals("stepDown")){
            p.down();
            makeStepPlayer(p, m);
        }
        else if(message.equals("take")){
            int newX = p.getNewX();
            int newY = p.getNewY();
            if(squares[newY][newX].GetKey()){
                p.keyIncrement();
                squares[newY][newX].newObject(new Path());
                //System.out.println(p.getName()+": take key");
                sendUpdateToAll(m);
                m = new CommandMessage("localMessage", "You just take KEY");
                p.sendMessage(m);
                //System.out.println("Vzal klic: "+p.getKeyCount());
                return true;
            }
            //System.out.println("Player: "+p.getName()+" can not take key");
            m = new CommandMessage("localMessage", "You can't take key");
            p.sendMessage(m);
            return false;
        }
        else if(message.equals("keys")){
            //System.out.println(p.getKeyCount());
            m.setCommand("keyCount");
            p.sendMessage(m);
            return true;
        }
        else if(message.equals("open")){
            int newX = p.getNewX();
            int newY = p.getNewY();
            if(p.getKeyCount() > 0 && squares[newY][newX].CanBeOpen()){
                squares[newY][newX].Open();
                p.keyDecrement();
                //System.out.println(p.getName()+" open gate");
                sendUpdateToAll(m);
                m = new CommandMessage("globalMessage", p.getName()+" open gate");
                sendUpdateToAll(m);
                return true;
                
            }
            else{
                //System.out.println(p.getName()+" can't open gate");
                m = new CommandMessage("localMessage", "You can't open gate");
                p.sendMessage(m);
                return false;
            }
        }
        return false;
    }
    /**
     * Check win situation
     * @return win situation
     * @throws IOException 
     */
    public boolean checkWin() throws IOException{
        
        for(Square s : finishBlock){
            if(s.getPlayer() != null){
                Player p =  s.getPlayer();
                gameRunning = false;
                //System.out.println("Player: " + p.getName() + " WIN");
                CommandMessage m = new CommandMessage("globalMessage", p.getName()+" win");
                for(Player p2: players){
                    p2.setEnd();
                }
                stopGameTime();
                sendUpdateToAll(m);
                m = new CommandMessage(this.id, 0, "winSituation", true);
                m.setGame(this);
                sendUpdateToAll(m); 
                
                return true; 
            }
        }
        return false;
    }
    
    /**
     * Player make step
     * @param p player
     * @param m command message to players
     * @throws IOException 
     */
    private void makeStepPlayer(Player p, CommandMessage m) throws IOException{
        int newX = p.getNewX();
        int newY = p.getNewY();
        CommandMessage m2;
        if(squares[newY][newX].CanSeize()){
            squares[newY][newX].Seize(squares[p.getY()][p.getX()].leave());
            p.makeStep();
            p.setPossition(newY, newX);
            //System.out.println(p.getName()+": make step");
            if(gameRunning)
                this.sendUpdateToAll(m);
            m = new CommandMessage("localMessage", "You make step");
            p.sendMessage(m);
            checkWin();
        }
        else{
            /* Player hit policeman - player die */
            if(squares[newY][newX].getEntity() != null){
                if(squares[newY][newX].getEntity().isPoliceman()){
                    m2 = new CommandMessage(this.id, 0, "YouDie", true);
                    p.setEnd();
                    m2.setGame(this);
                    p.sendMessage(m2);
                    players.remove(p);
                    squares[p.getY()][p.getY()].leave();
                    m2 = new CommandMessage("globalMessage", p.getName()+" die");
                    sendUpdateToAll(m2);
                    //System.out.println("You hit policeman");
                    if(gameRunning)
                        sendUpdateToAll(m);
                }
            }
            //System.out.println("Player: "+p.getName()+" can not make step");
            if(gameRunning)
                sendUpdateToAll(m);
            m = new CommandMessage("localMessage", "You can't make step");
            p.sendMessage(m);
        }
    }
    
    /**
     * Policeman make step
     * @param p policeman
     * @param m command message to players
     * @throws IOException 
     */
    private void makeStepPoliceman(Policeman p, CommandMessage m) throws IOException{
        int newX = p.getNewX();
        int newY = p.getNewY();
        CommandMessage m2;
        if(squares[newY][newX].CanSeize()){
            squares[newY][newX].Seize(squares[p.getY()][p.getX()].leave());
            p.setPossition(newY, newX);
            m.setGame(this);
            p.setPrevious(true);
        }
        else{
            /* Policeman hit player - player die */
            if(squares[newY][newX].getEntity() != null){
                if(squares[newY][newX].getEntity().isPlayer()){
                    Player player2 = squares[newY][newX].getPlayer();
                    squares[player2.getY()][player2.getX()].leave();
                    m2 = new CommandMessage(this.id, 0, "YouDie", true);
                    squares[newY][newX].Seize(squares[p.getY()][p.getX()].leave());
                    p.setPossition(newY, newX);
                    //System.out.println("Policeman kill player");
                    player2.setEnd();
                    m = new CommandMessage("globalMessage", player2.getName() + " die");
                    m2.setGame(this);
                    player2.sendMessage(m2);
                    players.remove(player2);
                    sendUpdateToAll(m);
                    p.setPrevious(true);
                    return;
                    
                }
            }
            p.setPrevious(false);
        }
    }
    /**
     * Send message to all playes in game
     * @param m message
     * @throws IOException 
     */
    private void sendUpdateToAll(CommandMessage m) throws IOException{
        for(Player p : players){
            p.sendMessage(m);
        }
    }
    
    /**
     * Get player object
     * @param id id of player
     * @return player
     */
    public Player getPlayer(int id){
        for(Player p : players){
            if(p.getID() == id)
                return p;
        }
        return null;
    }
    
    /**
     * Game name
     * @return game name
     */
    public String getGameName(){
        return this.name;
    }
    
    /**
     * Get squares
     * @return squares
     */
    public Square[][] getSquares(){
        return this.squares;
    }
    
    /**
     * Player leave game
     * @param p player
     * @throws IOException 
     */
    public void playerLeaveGame(Player p) throws IOException{
        squares[p.getY()][p.getX()].leave();
        players.remove(p);
        CommandMessage m = new CommandMessage("globalMessage", p.getName()+ " leave game");
        sendUpdateToAll(m);
        m = new CommandMessage(this.id, 0, "update", true);
        m.setGame(this);
        sendUpdateToAll(m);
        //System.out.println(p.getName()+ " leave game "+this.getGameName());
        freeSlots[p.getIdInGame()] = true;
        playerCount--;
    }
    
    /**
     * Check if game is empty
     * @return bool
     */
    public boolean isEmpty(){
        return players.isEmpty();
    }
    
    /**
     * Is player in game
     * @param pl player
     * @return boolean
     */
    public boolean playerInGame(Player pl){
        for(Player p : players){
            if(p.getID() == pl.getID())
                return true;
        }
        return false;
    }
    
    /**
     * Stop game time
     */
    public void stopGameTime(){
        gameTime = System.nanoTime() - startTime;
    }
    /**
     * Get game time
     * @return game time in second
     */
    public double getGameTime(){
        return gameTime/1000000000;
    }
}
