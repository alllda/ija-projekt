

package Entities;

import Gui.*;
import java.io.Serializable;

/**
 * Abstract class entity
 * @author Aleš Raszka
 */
public abstract class Entity implements Serializable{

    protected int x,y;
    protected int direction;
    public abstract void draw(DeskSquare panel);
    public abstract void textDraw();
    public abstract boolean isPlayer();
    public abstract boolean isPoliceman();
    public abstract Player getPlayer();
    public int getDirection(){
        return direction;
    }
    /**
     * Set possition
     * @param y y possition
     * @param x x possition
     */
    public void setPossition(int y, int x){
        this.x = x;
        this.y = y;
    }
    /**
     * Get new X
     * @return new X
     */
    public int getNewX(){
        if(direction == 1 || direction == 3)
            return x;
        else if(direction == 2)
            return x+1;
        else
            return x-1;
    }
    /**
     * Get new Y
     * @return new Y
     */
    public int getNewY(){
        if(direction == 2 || direction == 4)
            return y;
        else if(direction == 1)
            return y-1;
        else
            return y+1;
    }
    /**
     * Get x
     * @return x
     */
    public int getX(){
        return x;
    }
    /**
     * Get Y
     * @return y 
     */
    public int getY(){
        return y;
    }
    /**
     * Get this
     * @return this
     */
    public Entity getEntity(){
        return this;
    }
    /**
     * Turn right
     */
    public void turnRight(){
        if(direction == 4)
            direction = 1;
        else
            direction++;
    }
    /**
     * Turn left
     */
    public void turnLeft(){
        if(direction == 1)
            direction = 4;
        else
            direction--;
    }
    
}
