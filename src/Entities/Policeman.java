

package Entities;

import Gui.*;
import java.io.Serializable;

/**
 * Policeman class
 * @author Aleš Raszka
 */
public class Policeman extends Entity implements Serializable{
    private boolean previousSuccesful = false;
    public Policeman(){
        direction = 1;
    }
    @Override
    public void draw(DeskSquare panel) {
        if(this.direction == 1)
            panel.addEntity("examples/textures/policemanUp.png");
        else if(this.direction == 2)
            panel.addEntity("examples/textures/policemanRight.png");
        else if(this.direction == 3)
            panel.addEntity("examples/textures/policemanDown.png");
        else if(this.direction == 4)
            panel.addEntity("examples/textures/policemanLeft.png");
        else panel.addEntity("examples/textures/policemanRight.png");
    }
    
    @Override
    public void textDraw(){
        if(direction == 1)
            System.out.print("W");
        else if(direction == 2)
            System.out.print("D");
        else if(direction == 3)
            System.out.print("S");
        else if(direction == 4)
            System.out.print("A");
    }
    /**
     * Change direction
     */
    public void changeDirection(){
        int rand;
        if(previousSuccesful){
            // if previous was succesful (66% contionue / 33% turn)
            rand = 1 + (int)(Math.random() * ((6 - 1) + 1));
            switch(rand){
                case 1: 
                    turnLeft();
                    break;
                case 2:
                    turnRight();
                    break;
                default:
                    break;
            }
        }
        else{
            rand = 1 + (int)(Math.random() * ((2 - 1) + 1));
            switch(rand){
                case 1: 
                    turnLeft();
                    break;
                case 2:
                    turnRight();
                    break;
            }
        }
        //direction = 1 + (int)(Math.random() * ((4 - 1) + 1));  
    }
    /**
     * Is player
     * @return false
     */
    @Override
    public boolean isPlayer(){
        return false;
    }
    /**
     * Is policeman
     * @return true
     */
    @Override
    public boolean isPoliceman(){
        return true;
    }

    @Override
    public Player getPlayer() {
        return null;
    }
    /**
     * Set previous step
     * @param b 
     */
    public void setPrevious(Boolean b){
        previousSuccesful = b;
    }
    
}
