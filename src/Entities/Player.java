package Entities;

import Comunication.CommandMessage;
import Gui.*;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Player class
 * @author Aleš Raszka
 */
public class Player extends Entity implements Serializable{
    private final int id;
    private static int count = 0;
    private String name;
    
    private int keyCount;
    private boolean isGoing;
    private int steps;
    /* UP    = 1;
     * DOWN  = 3;
     * LEFT  = 4;
     * RIGHT = 2
     */
    transient ObjectOutputStream output;
    private boolean isConnected;
    private int idInGame;
    private long startTime;
    private long timeInGame;
    
    public Player(ObjectOutputStream output) throws IOException{
        this.output = output;
        this.keyCount = 0;
        this.steps = 0;
        id = count++;     
        direction = 3;
        this.name = "";
        output.writeObject(new Integer(id));
        output.flush();
        output.reset();
        this.isGoing = false;
        isConnected = true;
    }
    /**
     * Turn to left side
     */
    public void toLeft(){
        direction = 4;
    }
    /**
     * Turn to right side
     */
    public void toRight(){
        direction = 2;
    }
    /**
     * Turn up
     */
    public void up(){
        direction = 1;
    }
    /**
     * Turn down
     */
    public void down(){
        direction = 3;
    }
    /**
     * Set start time in game
     */
    public void setStartTime(){
        startTime = System.nanoTime();
    }
    /**
     * Get start time
     * @return start tiem
     */
    public long getStartTime(){
        return startTime;
    }
    /**
     * Set end
     */
    public void setEnd(){
        timeInGame = System.nanoTime() - startTime;
    }
    /**
     * Get total time 
     * @return total time in sec
     */
    public long getTimeInGame(){
        return (timeInGame/1000000000);
    }
    /**
     * Get name
     * @return name
     */
    public String getName(){
        return name;
    }
    /**
     * Set id in game
     * @param id id in game
     */
    public void setIdInGame(int id){
        idInGame = id;
    }
    /**
     * Get id in game
     * @return id in game
     */
    public int getIdInGame(){
        return idInGame;
    }
    /**
     * Disconect
     */
    public void disconect(){
        isConnected = false;
    }
    /**
     * Set name
     * @param name name 
     */
    public void setName(String name){
        this.name = name;
    }
    
    /**
     * Get ID
     * @return ID
     */
    public int getID(){
        return id;
    }    
    @Override
    public void draw(DeskSquare panel){
        if(direction == 1){
            switch(idInGame){
                case 0:
                    panel.addEntity("examples/textures/redUP.png");
                    break;
                case 1:
                    panel.addEntity("examples/textures/cyanUP.png");
                    break;
                case 2:
                    panel.addEntity("examples/textures/greenUP.png");
                    break;
                case 3:
                    panel.addEntity("examples/textures/orangeUP.png");
                    break;
                 
            }
        }
        else if(direction == 2){
            switch(idInGame){
                case 0:
                    panel.addEntity("examples/textures/redRIGHT.png");
                    break;
                case 1:
                    panel.addEntity("examples/textures/cyanRIGHT.png");
                    break;
                case 2:
                    panel.addEntity("examples/textures/greenRIGHT.png");
                    break;
                case 3:
                    panel.addEntity("examples/textures/orangeRIGHT.png");
                    break;
                 
            }
        }
        else if(direction == 3){
            switch(idInGame){
                case 0:
                    panel.addEntity("examples/textures/redDOWN.png");
                    break;
                case 1:
                    panel.addEntity("examples/textures/cyanDOWN.png");
                    break;
                case 2:
                    panel.addEntity("examples/textures/greenDOWN.png");
                    break;
                case 3:
                    panel.addEntity("examples/textures/orangeDOWN.png");
                    break;
                 
            }
        }
        else if(direction == 4){
            switch(idInGame){
                case 0:
                    panel.addEntity("examples/textures/redLEFT.png");
                    break;
                case 1:
                    panel.addEntity("examples/textures/cyanLEFT.png");
                    break;
                case 2:
                    panel.addEntity("examples/textures/greenLEFT.png");
                    break;
                case 3:
                    panel.addEntity("examples/textures/orangeLEFT.png");
                    break;
                 
            }
        }

        String text = new String("<html>"+"<b>Name: </b>"+this.getName()+"<br>"+
                "<b>Steps: </b>"+this.getSteps()+"<br>"+
                "<b>In Game: </b>"+(int)(this.getTimeInGame()/60)+":"+(this.getTimeInGame()%60)+" min"+"</html>");
        panel.addTooltip(text);
    }
    /**
     * Text draw to console
     */
    @Override
    public void textDraw(){
        if(direction == 1)
            System.out.print("^");
        else if(direction == 2)
            System.out.print(">");
        else if(direction == 3)
            System.out.print("v");
        else if(direction == 4)
            System.out.print("<");
    }
    /**
     * Increment key count
     */    
    public void keyIncrement(){
        keyCount++;
    }
    /**
     * Decrement key count
     */
    public void keyDecrement(){
        if(keyCount != 0)
            keyCount--;
    }
    /**
     * set key count to 0
     */
    public void resetkeyCount(){
        keyCount = 0;
    }
    /**
     * Get keyCount
     * @return keyCount
     */
    public int getKeyCount(){
        return keyCount;
    }
    /**
     * Go 
     */
    public void go(){
        this.isGoing = true;
    }
    /**
     * Stop
     */
    public void stop(){
        this.isGoing = false;
    }
    /**
     * Is in move
     * @return isGoing
     */
    public boolean isGoing(){
        return isGoing;
    }
    /**
     * Send message to client 
     * @param m message
     * @throws IOException 
     */
    public void sendMessage(CommandMessage m) throws IOException{
        //Send message to client        
        if(isConnected){
            setEnd();
            try{
                output.reset();
                output.writeObject(m);
            }catch(IOException e){
                //System.out.println("Odpojil se");
            }
            
        }
    }
    /**
     * nake step
     */
    public void makeStep(){
        steps++;
    }
    /**
     * get step count
     * @return steps
     */
    public int getSteps(){
        return steps;
    }
    /**
     * Set steps to 0
     */
    public void resetSteps(){
        steps = 0;
    }
    /**
     * Reset all..
     */
    public void resetAll(){
        resetSteps();
        resetkeyCount();
        stop();
        setStartTime();
    }
    /**
     * Is player
     * @return bool
     */
    @Override
    public boolean isPlayer(){
        return true;
    }
    /**
     * Is policeman
     * @return false
     */
    @Override
    public boolean isPoliceman(){
        return false;
    }
    /**
     * Get this
     * @return this
     */
    @Override
    public Player getPlayer(){
        return this;
    }
}
