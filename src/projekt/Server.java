package projekt;


import Comunication.*;
import Entities.Player;
import Games.Game;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Server class
 * @author Aleš Raszka
 */
public class Server {
    
    static ServerSocket serverSocket;
    List<Game> games = new ArrayList<>();
    List<Player> players = new ArrayList<>();
    public static void main(String[] argv) throws IOException {
        System.out.println("Set port: ");
        /*BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int port = Integer.parseInt(br.readLine());*/
        Server s = new Server();
        int port = 9999;
        
        serverSocket = new ServerSocket(port);
        
        while(true){
            System.out.println("Waiting for player");
            Socket socket = serverSocket.accept();
            System.out.println("Player connected, waiting for command");
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            
            Player p = new Player(out);
            s.getPlayersList().add(p);
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            ServerReceiver receiver = new ServerReceiver(s, in, out,p);
            receiver.start();
            
        }
        
        
    }
    /**
     * Get game from list by ID
     * @param id ID of game
     * @param games list of games
     * @return game object
     */
    public Game getGame(int id,List<Game> games){
        for(Game g : games){
            if(g.getID() == id)
                return g;
        }
        return null;
    }
    /**
     * Get player from list by ID
     * @param id id of player
     * @param players list of players
     * @return player object
     */
    public Player getPlayer(int id,List<Player> players){
        for(Player p : players){
            if(p.getID() == id)
                return p;
        }
        return null;
    }
    /**
     * Get game list
     * @return gameList
     */
    public List<Game> getGameList(){
        return games;
    }
    /**
     * Get Player list
     * @return player list
     */
    public List<Player> getPlayersList(){
        return players;
    }
    
}


