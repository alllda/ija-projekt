package projekt;


import Comunication.ClientReciever;
import Comunication.CommandMessage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import javax.swing.*;
import javax.swing.SwingUtilities;
import Gui.Gui;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Client class
 * @author Kamil Jeřábek
 */
public class Client {
    private final Gui frame;
    private ClientReciever reciever;
    private static Socket socket;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private String name;
    private static String servername;
    private static int port;
    private int ID;
    private int gameID;
    
    public static void main(String [] argv){
        SwingUtilities.invokeLater(new Runnable(){
            
            @Override
            public void run(){
                createAndShowGui();
            }
        }); 
    }
    
    /**
     * create and show whole gui
     */
    private static void createAndShowGui(){
        JFrame.setDefaultLookAndFeelDecorated(true);
        new Client();
    }
    
    /**
     * create gui
     */
    public Client(){
        gameID = -1;
        frame = new Gui(this);
    }
    
    /**
     * create connection to server
     * @param servername
     * @param port
     * @param name 
     */
    public void ConnectToServer(String servername, int port, String name){
        try {
            this.servername = servername;
            this.port = port;
            this.name = name;
            
            try{
                socket = new Socket(servername,port);
            }
            catch(UnknownHostException ex){
                System.exit(2);
            }
            
            in = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());
            this.ID = (Integer) in.readObject();
            out.flush();
            CommandMessage m = new CommandMessage(0, ID, "setName",false);
            m.setName(name);
            out.writeObject(m);
            reciever = new ClientReciever(this, in, out);
            reciever.start();
            
            
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(2);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(2);
        }
    }
    
    /**
     * get frame
     * @return frame
     */
    public Gui getFrame(){
        return this.frame;
    }
    
    /**
     * write to out stream
     * @param m 
     */
    public void typeOut(CommandMessage m){
        try {
            this.out.writeObject(m);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * get player ID
     * @return ID
     */
    public int getID(){
        return this.ID;
    }
    
    /**
     * get game id
     * @return gameID
     */
    public int getGameID(){
        return this.gameID;
    }

    /**
     * set game id
     * @param gameID 
     */
    public void setGameID(int gameID) {
        this.gameID = gameID;
    }
}