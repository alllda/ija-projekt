

package Comunication;

import Entities.Player;
import Games.Game;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import projekt.Server;

/**
 * Server receiver class
 * @author Aleš Raszka
 */
public class ServerReceiver extends Thread{
    
    private final ObjectInputStream in;
    private final ObjectOutputStream out;
    private final Server server;
    private Player player;
    
    public ServerReceiver(Server s, ObjectInputStream in, ObjectOutputStream out, Player p) throws IOException{
        this.server = s;
        this.in = in;
        this.out = out;
        this.player = p;
    }
    
    @Override
    public void run(){
        CommandMessage message = null;
        while(true){
            try {
                /** Get message from client */
                message = (CommandMessage) in.readObject();                
                
            } catch (IOException | ClassNotFoundException ex) {
                try {
                    //Logger.getLogger(ServerReceiver.class.getName()).log(Level.SEVERE, null, ex);
                    playerLeaveGame();
                } catch (IOException ex1) {
                    Logger.getLogger(ServerReceiver.class.getName()).log(Level.SEVERE, null, ex1);
                }
                break;
            }
            if(message == null)
                return;
            if(message.getCommand() != null){
                // Send commant to game
                if(message.isGameControl()){
                    Game g = server.getGame(message.getGameID(),server.getGameList());
                    try {
                        g.Command(message);
                    } catch (IOException ex) {
                        Logger.getLogger(ServerReceiver.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else{
                    if(message.getCommand().equals("quit")){
                        try {
                            playerLeaveGame();
                        } catch (IOException ex) {
                            Logger.getLogger(ServerReceiver.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }
                    else if(message.getCommand().equals("setName")){
                        server.getPlayer(message.getPlayerID(), server.getPlayersList()).setName(message.getName());
                    }
                    // New game
                    else if(message.getCommand().equals("game")){
                        Player p  = server.getPlayer(message.getPlayerID(), server.getPlayersList());
                        try {
                            Game g = new Game(message.getMap(), message.getName(), p, message.getRefreshTime());
                            server.getGameList().add(g);
                            g.start();
                        } catch (IOException ex) {
                            Logger.getLogger(ServerReceiver.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    // Joint to game
                    else if(message.getCommand().equals("join")){
                        Player p = server.getPlayer(message.getPlayerID(), server.getPlayersList());
                        Game g = server.getGame(message.getGameID(),server.getGameList());
                        try {
                            g.joinPlayer(p);
                        } catch (IOException ex) {
                            Logger.getLogger(ServerReceiver.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else if(message.getCommand().equals("leave")){
                        Player p = server.getPlayer(message.getPlayerID(), server.getPlayersList());
                        Game g = server.getGame(message.getGameID(),server.getGameList());
                        //System.out.println("LeaveGame");
                        try {
                            g.playerLeaveGame(p);
                        } catch (IOException ex) {
                            Logger.getLogger(ServerReceiver.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if(g.isEmpty()){
                            server.getGameList().remove(g);
                            //System.out.println("Vymazana");
                        }
                    }
                    // request for maps
                    else if(message.getCommand().equals("mapList")){
                        CommandMessage m = new CommandMessage(0, 0, "mapList", false);
                        try {
                            out.reset();
                            out.writeObject(m);
                        } catch (IOException ex) {
                            Logger.getLogger(ServerReceiver.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    // Request for games
                    else if(message.getCommand().equals("gameList")){
                        CommandMessage m = new CommandMessage(0, 0, "gameList", false);
                        m.setGamesList(server.getGameList());
                        //System.out.println("Pocet her: "+server.getGameList().size()+" ID: "+m.id);
                        
                        
                        try {
                            out.reset();
                            out.writeObject(m);
                        } catch (IOException ex) {
                            Logger.getLogger(ServerReceiver.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
                
            
        }
        player.disconect();
       
    }
    /**
     * Player leave game
     * @throws IOException 
     */
    private void playerLeaveGame() throws IOException{  
        for (Iterator<Game> g = server.getGameList().iterator(); g.hasNext(); ) {
            Game game = g.next();
            // Search for player in game
            if(game.playerInGame(player)){
                game.playerLeaveGame(player);
                if(game.isEmpty()){
                    g.remove();
                    //System.out.println("Hra vymazana");
                }
            }

        }
        
    }
    
    
}
