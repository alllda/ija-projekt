

package Comunication;

import Games.Game;
import java.io.Serializable;
import java.util.List;

/**
 * Commadn message class
 * @author Aleš Raszka
 */
public class CommandMessage implements Serializable{
    private final int player;
    private final int gameID;
    private String command;
    private String name;
    private String map;
    private int refreshTime;
    private final boolean gameControl;
    private final String [] mapsList;
    private List<Game> games;
    private Game game;
    private String message;
    public int id;
    
    public CommandMessage(int gameID, int player, String command, boolean gameControl){
        this.gameID = gameID;
        this.player = player;
        this.command = command;
        this.gameControl = gameControl;
        mapsList = new String[4];
        mapsList[0] = "examples/maps/25x25.txt";
        mapsList[1] = "examples/maps/mapa2.txt";
        mapsList[2] = "examples/maps/30x30.txt";
        mapsList[3] = "examples/maps/50x50.txt";
    }
    
    public CommandMessage(String command, String message){
        this.command = command;
        this.message = message;
        player = 0;
        gameID = 0;
        gameControl = false;
        mapsList = new String[4];
        mapsList[0] = "examples/maps/25x25.txt";
        mapsList[1] = "examples/maps/mapa2.txt";
        mapsList[2] = "examples/maps/30x30.txt";
        mapsList[3] = "examples/maps/50x50.txt";
    }
    /**
     * Is game control message?
     * @return bool
     */    
    public boolean isGameControl(){
        return gameControl;
    }
    /**
     * Get player from message
     * @return player
     */
    public int getPlayerID(){
        return player;
    }
    /**
     * Set name to message
     * @param name name
     */
    public void setName(String name){
        this.name = name;
    }
    /**
     * Get name 
     * @return name
     */
    public String getName(){
        return name;
    }
    /**
     * Set map
     * @param map map 
     */
    public void setMap(String map){
        this.map = map;
    }
    /**
     * Get map
     * @return map 
     */
    public String getMap(){
        return map;
    }
    /**
     * Get game ID
     * @return gameID
     */
    public int getGameID(){
        return gameID;
    }
    /**
     * Get command 
     * @return command
     */
    public String getCommand(){
        return command;
    }
    /**
     * Set refresh time
     * @param time refresh time in milisec
     */
    public void setRefreshTime(int time){
        refreshTime = time;
    }
    /**
     * Get refresh time
     * @return refresh time in milisec
     */
    public int getRefreshTime(){
        return refreshTime;
    }
    /**
     * Get map list from server
     * @return mapList
     */
    public String [] getMapsList(){
        return mapsList;
    }
    /**
     * Set gameList
     * @param g gameList
     */
    public void setGamesList(List<Game> g){
        games = g;
    }
    /**
     * Get gameList
     * @return gameList
     */
    public List<Game> getGamesList(){
        return games;
    }
    /**
     * Set Game
     * @param g game 
     */
    public void setGame(Game g){
        game = g;
    }
    /**
     * Get game 
     * @return game
     */
    public Game getGame(){
        return game;
    }
    /**
     * Set command
     * @param s command
     */
    public void setCommand(String s){
        command = s;
    }
    /**
     * Get message
     * @return message
     */
    public String getMessage(){
        return this.message;
    }
}
