package Comunication;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import projekt.Client;
import java.awt.Dimension;

/**
 * ClientReciever class
 * @author Kamil Jeřábek
 */
public class ClientReciever extends Thread {
    
    private final ObjectInputStream in;
    private final ObjectOutputStream out;
    private final Client client;
    
    public ClientReciever(Client client, ObjectInputStream in, ObjectOutputStream out){
        this.client = client;
        this.in = in;
        this.out = out;
        
    }
    
    @Override
    public void run(){
        CommandMessage message = null;
        boolean canBeUpdated = false;
        boolean canDie = false;
        while(true){
            try {
                message = (CommandMessage) in.readObject();                
                
            } catch (IOException | ClassNotFoundException ex ) {
                Logger.getLogger(ClientReciever.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(message == null)
                return;
            if(message.getCommand() != null){
                if(message.getCommand().equals("gameList")){
                    //System.out.println("gamelist");
                    client.getFrame().getJoinGamePanel().fillGameList(message);
                }
                else if(message.getCommand().equals("mapList")){
                    //System.out.println("maplist");
                    client.getFrame().getCreateGamePanel().fillMapList(message);
                }
                else if(message.getCommand().equals("newGame")){
                    canBeUpdated = true;
                    canDie = true;
                    //System.out.println("newGame");
                    client.setGameID(message.getGameID());
                    client.getFrame().getGamePanel().createAndShowGame(message);
                    client.getFrame().getCardLayoutGuiPanel().show(client.getFrame(), "game");
                    client.getFrame().getFrame().setSize(new Dimension(500,620));
                    client.getFrame().getGamePanel().getGameDeskPanel().requestFocusInWindow();
                }
                else if(message.getCommand().equals("update") && canBeUpdated){
                    //System.out.println("update");
                    client.getFrame().getGamePanel().getGameDeskPanel().repaintDesk(message);
                }
                else if(message.getCommand().equals("globalMessage")&& canBeUpdated){
                    //System.out.println("globalMessage");
                    client.getFrame().getGamePanel().addGlobalGameMessageTextArea(message.getMessage());
                }
                else if(message.getCommand().equals("localMessage")&& canBeUpdated){
                    //System.out.println("localMessage");
                    client.getFrame().getGamePanel().addIndividualGameMessageTextArea(message.getMessage());
                }
                else if(message.getCommand().equals("fullGame")){
                    //System.out.println("fullGame");
                    CommandMessage m = new CommandMessage(0,client.getID(),"gameList",false);
                    client.typeOut(m);
                }
                else if(message.getCommand().equals("winSituation") && canDie){
                    canBeUpdated = false;
                    canDie = false;
                    //System.out.println("winSituation");
                    client.getFrame().getGamePanel().getGameDeskPanel().showWin(message);
                }
                else if(message.getCommand().equals("YouDie") && canDie){
                    canBeUpdated = false;
                    canDie = false;
                    //System.out.println("YouDie");
                    client.getFrame().getGamePanel().getGameDeskPanel().showDie(message);
                }
            }
        }
    }  
}
                    
