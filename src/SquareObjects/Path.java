

package SquareObjects;

/**
 * Path class
 * @author Aleš Raszka
 */
public class Path extends SquareObject{
    public Path(){
        super("examples/textures/path.png",".");
    }
    
    @Override
    public boolean Open(){
        return false;
    }
    @Override
    public boolean CanSeize(){
        return true;
    }
    @Override
    public boolean GetKey(){
        return false;
    }
    @Override
    public boolean CanBeOpen(){
        return false;
    }
    
}
