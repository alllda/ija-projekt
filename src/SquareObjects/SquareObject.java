

package SquareObjects;

import Gui.*;
import java.io.Serializable;

/**
 * Abstract class SquareObject
 * @author Aleš Raszka
 */
public abstract class SquareObject implements Serializable{
    private String texture;
    private String textTexture;
    
    public SquareObject(String texture, String textTexture){
        this.texture = texture;
        this.textTexture = textTexture;
    }
    /**
     * Set texture
     * @param texture path to texture
     * @param textTexture text rexture
     */
    public void setTexture(String texture,String textTexture){
        this.texture = texture;
        this.textTexture = textTexture;
    }
    public abstract boolean Open();
    public abstract boolean CanSeize();
    public abstract boolean GetKey();
    public abstract boolean CanBeOpen();
    /**
     * Draw to GUI
     * @param panel gui panel 
     */
    public void draw(DeskSquare panel){
        panel.loadBackground(texture);
    }
    /**
     * TextDraw
     */
    public void textDraw(){
        System.out.print(textTexture);
    }
}
