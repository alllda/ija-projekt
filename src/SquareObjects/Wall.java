
package SquareObjects;

/**
 * Wall class
 * @author Aleš Raszka
 */
public class Wall extends SquareObject{
    
    public Wall(){
        super("examples/textures/wall.png","#");
    }
    
    @Override
    public boolean Open(){
        return false;
    }
    @Override
    public boolean CanSeize(){
        return false;
    }
    @Override
    public boolean GetKey(){
        return false;
    }
    @Override
    public boolean CanBeOpen(){
        return false;
    }
    
}
