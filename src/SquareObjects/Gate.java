

package SquareObjects;

/**
 * Gate class
 * @author allda
 */
public class Gate extends SquareObject{
    private boolean closed;
    public Gate(){
        super("examples/textures/gate.png","G");
        this.closed = true;
    }
    
    @Override
    public boolean Open(){
        //Open gate = change texture
        if(closed){
            closed = false;
            super.setTexture("examples/textures/openGate.png",":");
            return true;
        }
        else 
            return false;
    }
    @Override
    public boolean CanSeize(){
        return !closed;
    }
    @Override
    public boolean GetKey(){
        return false;
    }

    /**
     * Can be open
     * @return bool
     */
    @Override
    public boolean CanBeOpen(){
        return closed;
    }
}
