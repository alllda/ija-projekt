

package SquareObjects;

/**
 * Key class
 * @author Aleš Raszka
 */
public class Key extends SquareObject{
    public Key(){
        super("examples/textures/key.png","K");
    }
    
    @Override
    public boolean Open(){
        return false;
    }
    @Override
    public boolean CanSeize(){
        return false;
    }
    @Override
    public boolean GetKey(){
        return true;
    }
    @Override
    public boolean CanBeOpen(){
        return false;
    }
}
