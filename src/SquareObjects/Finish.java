

package SquareObjects;

/**
 * Finish class
 * @author Aleš Raszka
 */
public class Finish extends SquareObject{
    public Finish(){
        super("examples/textures/finish.png","F");
    }
    
    @Override
    public boolean Open(){
        return false;
    }
    @Override
    public boolean CanSeize(){
        return true;
    }
    @Override
    public boolean GetKey(){
        return false;
    }
    @Override
    public boolean CanBeOpen(){
        return false;
    }
}
